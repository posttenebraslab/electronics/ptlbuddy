use std::{cell::RefCell, fmt::Write as _};

use eframe::egui;
use embassy_executor::Executor;
use embassy_sync::{blocking_mutex::raw::CriticalSectionRawMutex, signal};
use embedded_graphics::{
    draw_target,
    pixelcolor::Rgb888,
    prelude::{OriginDimensions, Point, RgbColor, Size},
};
use reqwest::blocking::RequestBuilder;
use serde::de::DeserializeOwned;
use static_cell::StaticCell;

const IMAGE_SIZE: [usize; 2] = [graphics::DISPLAY_WIDTH, graphics::DISPLAY_HEIGHT];
const SERVER_URL: &str = env!("SERVER_URL");

type Signal<T> = signal::Signal<CriticalSectionRawMutex, T>;

#[derive(Default)]
struct MainStatic {
    wifi_connected: Signal<bool>,
    tap: Signal<Point>,

    badge: Signal<graphics::BadgeId>,
    barcode: Signal<graphics::BarcodeData>,
}
static MAIN_STATIC: StaticCell<MainStatic> = StaticCell::new();

struct SpawnStatic {
    executor: Executor,
    app: graphics::App<IO>,
}
static SPAWN_STATIC: StaticCell<SpawnStatic> = StaticCell::new();

struct IO {
    main: &'static MainStatic,
    client: reqwest::blocking::Client,
    ctx: egui::Context,
    render: RefCell<(FrameBuffer, egui::TextureHandle)>,
}
impl IO {
    async fn request<T: 'static + DeserializeOwned + Send>(&self, request: RequestBuilder) -> graphics::Result<T> {
        let signal = std::sync::Arc::new(Signal::new());
        let signal2 = signal.clone();
        std::thread::spawn(move || {
            let response = || {
                request.send()?.error_for_status()?.json::<T>()
            };
            signal2.signal(response());
        });
        let result = signal.wait().await;

        result.map_err(|err| {
            let mut error = graphics::Error::default();
            if err.is_connect() {
                write!(&mut error, "connect error")
            } else if err.is_builder() {
                panic!("{:?}", err);
            } else {
                write!(&mut error, "{}", err.without_url())
            }.unwrap();
            error
        })
    }
}
impl graphics::InputOutput for IO {
    type DrawTarget = FrameBuffer;

    async fn read_wifi_connected(&self) -> bool {
        self.main.wifi_connected.wait().await
    }
    async fn read_tap(&self) -> Point {
        self.main.tap.wait().await
    }

    async fn read_badge(&self) -> graphics::BadgeId {
        self.main.badge.wait().await
    }
    async fn read_barcode(&self) -> graphics::BarcodeData {
        self.main.barcode.wait().await
    }

    async fn search(&self, query: &str) -> graphics::Result<graphics::SearchResponse> {
        let url = format!("{SERVER_URL}/search?q={query}");
        let request = self.client.get(url);
        self.request(request).await
    }

    async fn transact(&self, user: &graphics::UserId, item: &graphics::ItemId) -> graphics::Result<graphics::User> {
        let url = format!("{SERVER_URL}/transact?user={user}&item={item}");
        let request = self.client.post(url);
        self.request(request).await
    }

    fn draw<T>(&self, drawable: impl FnOnce(&mut Self::DrawTarget) -> T) -> T {
        let render = &mut self.render.borrow_mut();
        let result = drawable(&mut render.0);
        let image = egui::ColorImage {
            size: IMAGE_SIZE,
            pixels: render.0.pixels.clone(),
        };
        render.1.set(image, egui::TextureOptions::NEAREST);
        self.ctx.request_repaint();
        result
    }
}

fn main() {
    let options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default()
            .with_resizable(false)
            .with_inner_size((840., 650.)),
        ..Default::default()
    };
    eframe::run_native(
        "Scaninator Simulator",
        options,
        Box::new(|cc| Ok(Box::new(App::new(cc)))),
    )
    .unwrap();
}

struct App {
    wifi_connected: bool,
    badge_id: String,
    barcode_data: String,
    texture_handle: egui::TextureHandle,
    main: &'static MainStatic,
}
impl App {
    fn new(cc: &eframe::CreationContext<'_>) -> Self {
        let main = MAIN_STATIC.init(MainStatic::default());
        let texture_handle = cc.egui_ctx.load_texture(
            "app",
            egui::ColorImage::new(IMAGE_SIZE, egui::Color32::BLACK),
            egui::TextureOptions::NEAREST,
        );
        let frame_buffer = FrameBuffer::new(
            egui::Color32::BLACK,
            graphics::DISPLAY_WIDTH,
            graphics::DISPLAY_HEIGHT,
        );

        let io = IO {
            main,
            client: Default::default(),
            ctx: cc.egui_ctx.clone(),
            render: (frame_buffer, texture_handle.clone()).into(),
        };
        std::thread::spawn(move || {
            let executor = Executor::new();
            let app = graphics::App::new(io);
            let spawn = SPAWN_STATIC.init(SpawnStatic { executor, app });

            spawn.executor.run(|spawner| {
                spawner.spawn(worker_task(&spawn.app)).unwrap();
                spawner.spawn(run_task(&spawn.app)).unwrap();
                spawner.spawn(animator_task(&spawn.app)).unwrap();
            });
        });

        Self {
            wifi_connected: false,
            badge_id: "%CA%75%FE%05".to_owned(),
            barcode_data: "4029764001807".to_owned(),
            texture_handle,
            main,
        }
    }
}
impl eframe::App for App {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::SidePanel::left("framebuffer").show(ctx, |ui| {
            const IMAGE_SCALE: f32 = 2.0;
            let mut texture = egui::load::SizedTexture::from_handle(&self.texture_handle);
            texture.size *= IMAGE_SCALE;
            let image = egui::Image::from_texture(texture);

            let image = image.sense(egui::Sense::click());
            let image = ui.add(image);
            if image.clicked() {
                let pos = image.interact_pointer_pos().unwrap() / IMAGE_SCALE;
                let point = Point::new(pos.x as i32, pos.y as i32);
                self.main.tap.signal(point);
            }
        });
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.heading("Simulator controller");
            ui.separator();

            egui::Grid::new("controller_grid")
                .num_columns(2)
                .spacing([20.0, 4.0])
                .striped(true)
                .show(ui, |ui| {
                    //////////
                    // WIFI //
                    //////////
                    ui.label("WiFi control:");
                    let button_text = match self.wifi_connected {
                        true => "Disconnect WiFi",
                        false => "Connect to WiFi",
                    };
                    if ui.button(button_text).clicked() {
                        self.wifi_connected = !self.wifi_connected;
                        self.main.wifi_connected.signal(self.wifi_connected);
                    }
                    ui.end_row();

                    //////////////
                    // BADGE ID //
                    //////////////
                    ui.label("Badge id:");
                    ui.with_layout(egui::Layout::right_to_left(egui::Align::Max), |ui| {
                        if ui.button("Send").clicked() {
                            self.main.badge.signal(self.badge_id.as_str().into());
                        }
                        ui.add_sized(
                            ui.available_size(),
                            egui::TextEdit::singleline(&mut self.badge_id)
                                .hint_text("Base64url encoded badge id")
                                .char_limit(graphics::BadgeId::CAPACITY)
                                .desired_width(ui.available_width()),
                        );
                    });
                    ui.end_row();

                    /////////////
                    // BARCODE //
                    /////////////
                    ui.label("Barcode data:");
                    ui.with_layout(egui::Layout::right_to_left(egui::Align::Max), |ui| {
                        if ui.button("Send").clicked() {
                            self.main
                                .barcode
                                .signal(self.barcode_data.as_str().into());
                        }
                        ui.add_sized(
                            ui.available_size(),
                            egui::TextEdit::singleline(&mut self.barcode_data)
                                .hint_text("Base64url encoded barcode data")
                                .char_limit(graphics::BarcodeData::CAPACITY)
                                .desired_width(ui.available_width()),
                        );
                    });
                    ui.end_row();
                });
        });
    }
}

#[embassy_executor::task]
async fn worker_task(app: &'static graphics::App<IO>) -> ! {
    app.worker().await;
}

#[embassy_executor::task]
async fn run_task(app: &'static graphics::App<IO>) -> ! {
    app.run().await;
}

#[embassy_executor::task]
async fn animator_task(app: &'static graphics::App<IO>) -> ! {
    app.animator().await;
}

struct FrameBuffer {
    pixels: Vec<egui::Color32>,
    width: usize,
    height: usize,
}
impl FrameBuffer {
    fn new(color: egui::Color32, width: usize, height: usize) -> Self {
        let pixels = vec![color; width * height];
        Self {
            pixels,
            width,
            height,
        }
    }
    fn point_to_index(&self, point: Point) -> usize {
        self.width * point.y as usize + point.x as usize
    }
}
impl OriginDimensions for FrameBuffer {
    fn size(&self) -> Size {
        Size::new(self.width as u32, self.height as u32)
    }
}
impl draw_target::DrawTarget for FrameBuffer {
    type Color = graphics::Color;
    type Error = &'static str;
    fn draw_iter<I>(&mut self, pixels: I) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = embedded_graphics::Pixel<Self::Color>>,
    {
        for pixel in pixels {
            let index = self.point_to_index(pixel.0);
            let rgb888 = Rgb888::from(pixel.1);
            let color32 = egui::Color32::from_rgb(rgb888.r(), rgb888.g(), rgb888.b());
            self.pixels[index] = color32
        }
        Ok(())
    }
}
