use embedded_graphics::prelude::RgbColor;
use palette::{FromColor, Hsl, Srgb};

use crate::Color;

pub fn rgb_to_hsl(rgb: Color) -> Hsl {
    let rgb = Srgb::new(
        rgb.r() as f32 / Color::MAX_R as f32,
        rgb.g() as f32 / Color::MAX_G as f32,
        rgb.b() as f32 / Color::MAX_B as f32,
    );
    Hsl::from_color(rgb)
}

pub fn hsl_to_rgb(hsv: Hsl) -> Color {
    let rgb = Srgb::from_color(hsv);
    Color::new(
        (rgb.red * Color::MAX_R as f32) as u8,
        (rgb.green * Color::MAX_G as f32) as u8,
        (rgb.blue * Color::MAX_B as f32) as u8,
    )
}
