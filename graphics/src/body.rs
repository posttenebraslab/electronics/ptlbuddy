use core::{fmt::Write as _, marker::PhantomData};

use embedded_graphics::{mono_font::{MonoFont, MonoTextStyle}, prelude::{OriginDimensions, Point, RgbColor, Size}, primitives::Rectangle, Drawable as _};
use embedded_text::{alignment::VerticalAlignment, style::TextBoxStyleBuilder, TextBox};

use crate::{Color, DrawTarget, DISPLAY_HEIGHT, DISPLAY_WIDTH, HEADER_HEIGHT};

type DefaultStyle = MonoTextStyle<'static, Color>;

const FONT: MonoFont = profont::PROFONT_12_POINT;
const CHAR_SIZE: Size = FONT.character_size;
const CHAR_HEIGHT: usize = CHAR_SIZE.height as usize;
const CHAR_WIDTH: usize = CHAR_SIZE.width as usize;
const DEFAULT_TEXT_STYLE: DefaultStyle = DefaultStyle::new(&FONT, Color::WHITE);
const BODY_HEIGHT: usize = DISPLAY_HEIGHT - HEADER_HEIGHT;
const BODY_LINES: usize = BODY_HEIGHT / CHAR_HEIGHT;
const BODY_COLUMNS: usize = DISPLAY_WIDTH / CHAR_WIDTH;

const LOG_LINES: usize = BODY_LINES - 3;
const LOG_BUFFER_MAX_BYTES: usize = (BODY_COLUMNS * char::MAX.len_utf8() + "\n".as_bytes().len()) * LOG_LINES;

#[derive(Default)]
pub struct LogBuffer<const N: usize>(circular_buffer::CircularBuffer<N, u8>);
impl<const N: usize> core::fmt::Write for LogBuffer<N> {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        self.0.extend_from_slice(s.as_bytes());
        while let Some(front) = self.0.front() && (128..192).contains(front) {
            self.0.pop_front();
        }
        Ok(())
    }
}

#[derive(Default)]
pub enum Status {
    #[default]
    WaitBoth,
    WaitUser,
    WaitItem,
    Busy,
}

pub struct Body<D: DrawTarget> {
    _d: PhantomData<D>,
    log: LogBuffer<LOG_BUFFER_MAX_BYTES>,
}

impl<D: DrawTarget> Body<D> {

    pub fn new(_target: &mut D) -> Self {
        Self {
            _d: Default::default(),
            log: Default::default(),
        }
    }

    fn rectangle(x: usize, y: usize, width: usize, height: usize) -> Rectangle {
        Rectangle::new(Point::new(x as i32, y as i32), Size::new(width as u32, height as u32))
    }

    pub fn update_log(&mut self, target: &mut D, logger: impl FnOnce(&mut LogBuffer<LOG_BUFFER_MAX_BYTES>) -> core::fmt::Result) {
        writeln!(&mut self.log).unwrap();
        logger(&mut self.log).unwrap();

        let bytes = self.log.0.make_contiguous();
        let text = core::str::from_utf8(bytes).unwrap();

        let bounds = Self::rectangle(0, HEADER_HEIGHT, DISPLAY_WIDTH, CHAR_HEIGHT * LOG_LINES);
        target.fill_solid(&bounds, Color::BLACK).unwrap();
        let style = TextBoxStyleBuilder::new().vertical_alignment(VerticalAlignment::Bottom).build();
        TextBox::with_textbox_style(text, bounds, DEFAULT_TEXT_STYLE, style).draw(target).unwrap();
    }

    pub fn update_status(&mut self, target: &mut D, status: Status) {
        let status = match status {
            Status::Busy => "Please wait…",
            Status::WaitBoth => "Please scan a badge/barcode.",
            Status::WaitItem => "Please scan an item.",
            Status::WaitUser => "Please scan a user badge.",
        };

        let bounds = Self::rectangle(0, DISPLAY_HEIGHT - HEADER_HEIGHT - CHAR_HEIGHT, DISPLAY_WIDTH, CHAR_HEIGHT);
        target.fill_solid(&bounds, Color::BLACK).unwrap();
        TextBox::new(status, bounds, DEFAULT_TEXT_STYLE).draw(target).unwrap();
    }

}

impl<D: DrawTarget> OriginDimensions for Body<D> {
    fn size(&self) -> Size {
        let line_height = DEFAULT_TEXT_STYLE.font.character_size.height;
        let height = line_height * 3;
        Size::new(DISPLAY_WIDTH as u32, height)
    }
}
