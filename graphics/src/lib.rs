#![cfg_attr(not(test), no_std)]
#![feature(let_chains, trait_alias, round_char_boundary)]

mod body;
mod header;
mod string;
mod tools;

use core::{cell::RefCell, fmt::{Display, Write as _}};
use embassy_futures::select::{Either, Either3, select, select3};
use embassy_sync::{blocking_mutex::raw::NoopRawMutex, signal::Signal};
use embassy_time::{Duration, Instant, Timer};
use embedded_graphics::{
    draw_target, geometry::Dimensions, pixelcolor, prelude::{Point, RgbColor as _}
};
use serde::{Deserialize, Serialize};
use string::String;

pub type Color = pixelcolor::Rgb565;
pub trait DrawTarget = draw_target::DrawTarget<Color = Color, Error: core::fmt::Debug>;

pub const DISPLAY_WIDTH: usize = 240;
pub const DISPLAY_HEIGHT: usize = 320;
pub const HEADER_HEIGHT: usize = 36;

pub type BadgeId = String<31>;
pub type BarcodeData = String<32>;
pub type Error = String<123>;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Cents(i16);
impl Display for Cents {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{}{:01}.{:02}", if self.0.is_negative() { "-" } else { "" }, self.0.abs() / 100, self.0.abs() % 100)
    }
}

pub type UserId = String<34>;
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct User {
    pub id: UserId,
    pub name: String<34>,
    pub balance: Cents,
}

pub type ItemId = String<34>;
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Item {
    pub id: ItemId,
    pub name: String<35>,
    pub value: Cents,
}

#[derive(Clone, Debug)]
pub enum Read {
    Badge(BadgeId),
    Barcode(BarcodeData),
}
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum SearchResponse {
    User(User),
    Item(Item),
}
pub type Result<T> = core::result::Result<T, Error>;

pub trait InputOutput {
    type DrawTarget: DrawTarget;

    fn read_wifi_connected(&self) -> impl Future<Output = bool>;
    fn read_tap(&self) -> impl Future<Output = Point>;

    fn read_badge(&self) -> impl Future<Output = BadgeId>;
    fn read_barcode(&self) -> impl Future<Output = BarcodeData>;

    fn search(&self, query: &str) -> impl Future<Output = Result<SearchResponse>>;
    fn transact(&self, user: &UserId, item: &ItemId) -> impl Future<Output = Result<User>>;

    fn draw<T>(&self, drawable: impl FnOnce(&mut Self::DrawTarget) -> T) -> T;
}

#[derive(Default)]
enum Status {
    #[default]
    HaveNothing,
    HaveUser(UserId),
    HaveItem(ItemId),
}

struct AppState<D: DrawTarget> {
    header: header::Header<D>,
    body: body::Body<D>,
    status: Status,
}

impl<D: DrawTarget> AppState<D> {

    fn new(target: &mut D) -> Self {
        target.clear(Color::BLACK).unwrap();
        Self {
            header: header::Header::new(target),
            body: body::Body::new(target),
            status: Default::default(),
        }
    }

    fn update_tap(&mut self, _target: &mut D, point: Point) {
        if self.header.bounding_box().contains(point) {
        } else if self.body.bounding_box().contains(point) {
        }
    }

}


pub struct App<IO: InputOutput> {
    io: IO,
    state: RefCell<AppState<IO::DrawTarget>>,
    busy: Signal<NoopRawMutex, bool>,
}

impl<IO: InputOutput> App<IO> {

    pub fn new(io: IO) -> Self {
        let state = io.draw(|target| AppState::new(target)).into();
        Self {
            io,
            state,
            busy: Default::default(),
        }
    }

    fn update<T>(&self, updater: impl FnOnce(&mut AppState<IO::DrawTarget>, &mut IO::DrawTarget) -> T) -> T {
        self.io.draw(|target| updater(&mut self.state.borrow_mut(), target))
    }

    pub async fn wait_background(&self) {
        match select(self.io.read_wifi_connected(), self.io.read_tap()).await {
            Either::First(wifi_connected) => {
                self.update(|s, t| s.header.update_wifi_connected(t, wifi_connected))
            }
            Either::Second(tap) => self.update(|s, t| s.update_tap(t, tap)),
        }
    }

    pub async fn wait_read(&self) -> Read {
        match select(self.io.read_badge(), self.io.read_barcode()).await {
            Either::First(badge_id) => Read::Badge(badge_id),
            Either::Second(barcode_data) => Read::Barcode(barcode_data),
        }
    }

    pub async fn wait_timeout(&self) {
        const IDLE_TIMEOUT: Duration = Duration::from_secs(30);
        Timer::after(IDLE_TIMEOUT).await
    }

    pub async fn run(&self) -> ! {
        loop {
            'idle: loop {
                loop {
                    match select3(
                        self.wait_background(),
                        self.busy.wait(),
                        self.wait_timeout(),
                    )
                    .await
                    {
                        Either3::First(()) => {}
                        Either3::Second(busy) => if busy { break 'idle },
                        Either3::Third(()) => break,
                    };
                }

                // timeout
                self.update(|s, t| {
                    s.status = Default::default();
                    s.body.update_log(t, |log| writeln!(log, "Timeout!"));
                    s.body.update_status(t, body::Status::default());
                });

                match select(self.wait_background(), self.busy.wait()).await {
                    Either::First(()) => {}
                    Either::Second(busy) => if busy { break 'idle },
                }
            };

            'busy: loop {
                match select(self.wait_background(), self.busy.wait()).await {
                    Either::First(()) => {}
                    Either::Second(busy) => if !busy { break 'busy },
                };
            };
        }
    }

    pub async fn worker(&self) -> ! {
        loop {
            self.update(|s, t| s.body.update_status(t, match s.status {
                Status::HaveUser(_) => body::Status::WaitItem,
                Status::HaveItem(_) => body::Status::WaitUser,
                Status::HaveNothing => body::Status::WaitBoth,
            }));

            self.busy.signal(false);
            let read = self.wait_read().await;
            self.busy.signal(true);

            let query = self.update(|s, t| match &read {
                Read::Badge(badge_id) => {
                    let badge_id = badge_id.0.as_str();
                    s.body.update_log(t, |log| writeln!(log, "Looking up badge {badge_id}..."));
                    badge_id
                },
                Read::Barcode(barcode_data) => {
                    let barcode_data = barcode_data.0.as_str();
                    s.body.update_log(t, |log| writeln!(log, "Looking up barcode {barcode_data}..."));
                    barcode_data
                },
            });

            self.update(|s, t| s.body.update_status(t, body::Status::Busy));

            let result = self.io.search(query).await;

            let transaction = self.update(|s, t| {
                match result {
                    Ok(SearchResponse::User(user)) => {
                        s.body.update_log(t, |log| writeln!(log, "Found user\n{name} ({id})\nBalance {balance}",
                            id=user.id,
                            name=user.name,
                            balance=user.balance,
                        ));
                        let status = core::mem::replace(&mut s.status, Status::HaveUser(user.id.clone()));
                        if let Status::HaveItem(item_id) = status {
                            Some((user.id, item_id))
                        } else {
                            None
                        }
                    },
                    Ok(SearchResponse::Item(item)) => {
                        s.body.update_log(t, |log| writeln!(log, "Found item\n{name} ({id})\nValue {value}",
                            id=item.id,
                            name=item.name,
                            value=item.value,
                        ));
                        match &mut s.status {
                            Status::HaveUser(user_id) => {
                                Some((user_id.clone(), item.id))
                            },
                            _ => {
                                s.status = Status::HaveItem(item.id);
                                None
                            },
                        }
                    },
                    Err(error) => {
                        s.body.update_log(t, |log| writeln!(log, "{error}"));
                        None
                    },
                }
            });

            let Some((user_id, item_id)) = transaction else {continue};

            let result = self.io.transact(&user_id, &item_id).await;
            match result {
                Err(error) => {
                    self.update(|s, t| s.body.update_log(t, |log| writeln!(log, "{error}")));
                    continue
                },
                Ok(user) => self.update(|s, t| {
                    s.body.update_log(t, |log| writeln!(log, "Bought!\n{name} ({id})\nBalance {balance}",
                        id=user.id,
                        name=user.name,
                        balance=user.balance,
                    ));
                }),
            }
        }
    }

    pub async fn animator(&self) -> ! {
        loop {
            Timer::after_millis(20).await;
            self.update(|s, t| s.header.update_animation(t, Instant::now()));
        }
    }
}
