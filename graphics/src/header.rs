use core::marker::PhantomData;

use embassy_time::Instant;
use embedded_graphics::{draw_target::DrawTargetExt as _, geometry::{AnchorPoint, OriginDimensions}, image::ImageDrawable, mono_font::{MonoFont, MonoTextStyle}, prelude::{Point, RgbColor, Size}, text::{renderer::CharacterStyle, Text}, Drawable};
use palette::{Hsl, ShiftHue};

use crate::{tools, Color, DrawTarget, DISPLAY_WIDTH, HEADER_HEIGHT};

const FONT: MonoFont = profont::PROFONT_24_POINT;

// TODO: use lazy_static if possible with no_std
const WIFI_CONNECTED: &[u8] = include_bytes!("./images/wifi-connected.rgb565.bmp");
const WIFI_DISCONNECTED: &[u8] = include_bytes!("./images/wifi-disconnected.rgb565.bmp");

type Bmp = tinybmp::Bmp<'static, Color>;

pub struct Header<D: DrawTarget> {
    _d: PhantomData<D>,
    text: Text<'static, MonoTextStyle<'static, Color>>,
    initial_time: Instant,
    initial_color: Hsl,
}

impl<D: DrawTarget> OriginDimensions for Header<D> {
    fn size(&self) -> Size {
        Size::new(DISPLAY_WIDTH as u32, HEADER_HEIGHT as u32)
    }
}

impl<D: DrawTarget> Header<D> {

    pub fn new(target: &mut D) -> Self {
        let initial_time = Instant::now();
        let initial_color = Color::RED;
        let text = Text::new(
            "Scaninator",
            Point::new(0, FONT.baseline as i32),
            MonoTextStyle::new(&FONT, initial_color),
        );
        let initial_color = tools::rgb_to_hsl(initial_color);

        let mut new = Self {
            _d: Default::default(),
            text,
            initial_time,
            initial_color,
        };
        new.update_animation(target, initial_time);
        new.update_wifi_connected(target, false);
        new
    }

    pub fn update_animation(&mut self, target: &mut D, now: Instant) {
        let hue_shift = (now - self.initial_time).as_millis() as f32 / 10.;
        let text_color = self.initial_color.shift_hue(hue_shift);
        self.text.character_style.set_text_color(Some(tools::hsl_to_rgb(text_color)));
        self.text.draw(target).unwrap();
    }

    pub fn update_wifi_connected(&mut self, target: &mut D, wifi_connected: bool) {
        let icon_bytes = match wifi_connected {
            true => WIFI_CONNECTED,
            false => WIFI_DISCONNECTED,
        };
        let icon_bmp = Bmp::from_slice(icon_bytes).unwrap();

        let top_right = target.bounding_box().anchor_point(AnchorPoint::TopRight);
        let translation = top_right - icon_bmp.size().x_axis();
        let target = &mut target.translated(translation);
        icon_bmp.draw(target).unwrap();
    }

}
