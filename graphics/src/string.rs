use serde::{Deserialize, Serialize};


fn str_truncate(string: &str, length: usize) -> &str {
    &string[..string.floor_char_boundary(length)]
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct String<const N: usize>(pub heapless::String<N>);
impl<const N: usize> String<N> {
    pub const CAPACITY: usize = N;
}
impl<const N: usize> core::fmt::Display for String<N> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_str(self.into())
    }
}
impl<const N: usize> core::fmt::Write for String<N> {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        self.0.push_str(str_truncate(s, N - self.0.len())).unwrap();
        Ok(())
    }
}
impl<const N: usize> From<&str> for String<N> {
    fn from(value: &str) -> Self {
        Self(heapless::String::try_from(str_truncate(value, N)).unwrap())
    }
}
impl<'a, const N: usize> Into<&'a str> for &'a String<N> {
    fn into(self) -> &'a str {
        self.0.as_str()
    }
}
