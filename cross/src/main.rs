#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![feature(type_changing_struct_update)]

use core::cell::RefCell;
use core::mem::MaybeUninit;

use embassy_net::tcp::TcpSocket;
use embassy_executor::Spawner;
use embassy_net::{Config, Runner, Stack, StackResources};
use embassy_sync::{blocking_mutex::raw::{CriticalSectionRawMutex, NoopRawMutex}, signal};
use embedded_graphics::pixelcolor::{Rgb565, Rgb666};
use embedded_graphics::prelude::{Point, RgbColor};
use embedded_hal_bus::spi::ExclusiveDevice;
use embedded_io_async::{Read, Write as _};
use embedded_svc::io::asynch::Write as _;
use esp_hal::clock::CpuClock;
use esp_hal::delay::Delay;
use esp_hal::gpio::{Io, Level, OutputConfig};
use esp_hal::interrupt::software::SoftwareInterrupt;
use esp_hal::peripherals::Peripherals;
use esp_hal::rng::Rng;
use esp_hal::spi::master::Spi;
use esp_hal::timer::timg::TimerGroup;
use esp_hal::uart::{AtCmdConfig, Uart};
use esp_hal::{self as hal, main, time::Rate,};

use embassy_sync::channel::{Channel, Sender};
use embassy_time::{Duration, Instant, Timer};
use esp_backtrace as _;
use esp_hal_embassy::{Executor, InterruptExecutor};
use esp_println::{print, println};
use esp_wifi::wifi::{ClientConfiguration, Configuration, WifiController, WifiDevice, WifiEvent, WifiState};
use esp_wifi::EspWifiController;
use hal::interrupt::Priority;
use hal::peripherals::UART2;
use hal::{
    gpio::{
        GpioPin,
        Output,
    },
    peripherals::SPI2,
};
use mipidsi::interface::SpiInterface;
use mipidsi::options::{ColorInversion, Orientation, Rotation};
use mipidsi::{models::ST7789, Builder};
use static_cell::{make_static, StaticCell};

const SSID: &str = "PTL-N";
const PASSWORD: &str = "P0stT3n3br4sL4b";

const READ_BUF_SIZE: usize = 64;

static SCREEN_BUFFER: static_cell::StaticCell<[u8; 512]> = static_cell::StaticCell::new();

type Display = mipidsi::Display<SpiInterface<'static, ExclusiveDevice<Spi<'static, esp_hal::Async>, NoCs, embedded_hal_bus::spi::NoDelay>, Output<'static>>, ST7789, mipidsi::NoResetPin>;

type Signal<T> = signal::Signal<NoopRawMutex, T>;
#[derive(Default)]
struct Signals {
    wifi_connected: Signal<bool>,
    tap: Signal<Point>,

    badge: Signal<graphics::BadgeId>,
    barcode: Signal<graphics::BarcodeData>,
}
struct AppIO {
    signals: &'static Signals,
    display: RefCell<Display>,
}
impl graphics::InputOutput for AppIO {
    type DrawTarget = Display;

    async fn read_wifi_connected(&self) -> bool {
        self.signals.wifi_connected.wait().await
    }
    async fn read_tap(&self) -> Point {
        self.signals.tap.wait().await
    }

    async fn read_badge(&self) -> graphics::BadgeId {
        self.signals.badge.wait().await
    }
    async fn read_barcode(&self) -> graphics::BarcodeData {
        self.signals.barcode.wait().await
    }

    async fn search(&self, query: &str) -> graphics::Result<graphics::SearchResponse> {
        Err("TODO!".into())
    }

    async fn transact(&self, user: &graphics::UserId, item: &graphics::ItemId) -> graphics::Result<graphics::User> {
        Err("TODO!".into())
    }

    fn draw<T>(&self, drawable: impl FnOnce(&mut Self::DrawTarget) -> T) -> T {
        let display = &mut self.display.borrow_mut();
        drawable(display)
    }

}
type App = graphics::App<AppIO>;

macro_rules! mk_static {
    ($t:ty,$val:expr) => {{
        static STATIC_CELL: static_cell::StaticCell<$t> = static_cell::StaticCell::new();
        #[deny(unused_attributes)]
        let x = STATIC_CELL.uninit().write(($val));
        x
    }};
}

// static EXECUTOR: StaticCell<Executor> = StaticCell::new();
// static DISPLAY_CHANNEL: Channel<CriticalSectionRawMutex, graphics::Events, 10> = Channel::new();
// static mut APP_CORE_STACK: hal::cpu_control::Stack<32768> = hal::cpu_control::Stack::new();

// static INT_EXECUTOR_CORE_0: InterruptExecutor<0> = InterruptExecutor::new(unsafe {SoftwareInterrupt::<0>::steal()});
// static INT_EXECUTOR_CORE_1: InterruptExecutor<1> = InterruptExecutor::new(unsafe {SoftwareInterrupt::<0>::steal()});

// #[interrupt]
// fn FROM_CPU_INTR1() {
//     unsafe { INT_EXECUTOR_CORE_0.on_interrupt() }
// }

// #[interrupt]
// fn FROM_CPU_INTR2() {
//     unsafe { INT_EXECUTOR_CORE_1.on_interrupt() }
// }


#[esp_hal_embassy::main]
async fn main(spawner: Spawner) -> ! {
    // #[cfg(feature = "log")]
    // esp_println::logger::init_logger(log::LevelFilter::Info);

    let config = esp_hal::Config::default().with_cpu_clock(CpuClock::max());
    let peripherals = esp_hal::init(config);

    esp_alloc::heap_allocator!(size: 72 * 1024);

    let timg0 = TimerGroup::new(peripherals.TIMG0);
    let mut rng = Rng::new(peripherals.RNG);

    let timer = timg0.timer0;

    // let init = &*mk_static!(
    //     EspWifiController<'static>,
    //     esp_wifi::init(timer, rng.clone(), peripherals.RADIO_CLK).unwrap()
    // );

    // let wifi = peripherals.WIFI;
    // let (wifi_interface, controller) =
    //     esp_wifi::wifi::new(&init, wifi).unwrap();


    let esp_wifi_ctrl = &*mk_static!(
        EspWifiController<'static>,
        esp_wifi::init(timer, rng.clone(), peripherals.RADIO_CLK).unwrap()
    );

    let (controller, interfaces) = esp_wifi::wifi::new(&esp_wifi_ctrl, peripherals.WIFI).unwrap();

    let wifi_interface = interfaces.sta;

    let timg1 = TimerGroup::new(peripherals.TIMG1);
    esp_hal_embassy::init(timg1.timer0);

    let config = embassy_net::Config::dhcpv4(Default::default());

    let seed = (rng.random() as u64) << 32 | rng.random() as u64;

    // Init network stack
    let (stack, runner) = embassy_net::new(
        wifi_interface,
        config,
        mk_static!(StackResources<3>, StackResources::<3>::new()),
        seed,
    );

    spawner.spawn(connection(controller)).ok();
    spawner.spawn(net_task(runner)).ok();

    let delay = Delay::new();

    
    hal::interrupt::enable(
        hal::peripherals::Interrupt::SPI2_DMA,
        hal::interrupt::Priority::Priority1,
    )
    .unwrap();

    // Prepare GPIOs
    let config = OutputConfig::default();
    let io = Io::new(peripherals.IO_MUX);
    let led = Output::new(peripherals.GPIO2, Level::High, config);


    // SPI for screen
    let sclk = peripherals.GPIO18;
    let mosi = peripherals.GPIO23;
    let cs = peripherals.GPIO12;

    let spi = Spi::new(
        peripherals.SPI2,
        hal::spi::master::Config::default()
            .with_frequency(Rate::from_mhz(60))
            .with_mode(hal::spi::Mode::_0),
    )
    .unwrap()
    .with_sck(sclk)
    .with_mosi(mosi) // order matters
    .with_cs(cs)
    .into_async()    ;

    let dc = Output::new(peripherals.GPIO27, Level::Low, config);
    let _rst = Output::new(peripherals.GPIO14, Level::High, config);
    let _bl = Output::new(peripherals.GPIO19, Level::High, config);


    // let config = hal::uart::Config::default().with_baudrate(115_200);

    // let mut uart0 = Uart::new(peripherals.UART0, config)
    //     .unwrap()
    //     .with_tx(peripherals.GPIO17)
    //     .with_rx(peripherals.GPIO16);

    // uart0.set_at_cmd(AtCmdConfig::default().with_cmd_char(0x03));

    // // const AT_CMD: u8 = 0x03;
    // // const READ_BUF_SIZE: usize = 64;
    // // uart.set_at_cmd(AtCmdConfig::new(None, None, None, AT_CMD, None));
    // // uart.set_rx_fifo_full_threshold(1).unwrap();
    // // uart.listen_at_cmd();

    // esp32_hal::interrupt::enable(
    //     esp32_hal::peripherals::Interrupt::UART2,
    //     esp32_hal::interrupt::Priority::Priority1,
    // )
    // .unwrap();

    // SPI Display
    let spi_device = ExclusiveDevice::new_no_delay(spi, NoCs).unwrap();
    let buffer = mk_static!([u8; 2048], [0_u8; 2048]);
    let di = SpiInterface::new(spi_device, dc, buffer);
    let mut delay = Delay::new();
    let display = Builder::new(ST7789, di)
        .display_size(240, 320)
        // .orientation(Orientation::new().rotate(Rotation::Deg270).flip_vertical())
        // .invert_colors(ColorInversion::Inverted)
        .init(&mut delay)
        .unwrap();


    println!("Init done!");

    // let display_events_rx = DISPLAY_CHANNEL.receiver();
    // let display_events_tx = DISPLAY_CHANNEL.sender();

    // let mut display = graphics::Display::new(display, display_events_rx);
    // display.init().unwrap();

    // // Init network stack
    // let config = Config::dhcpv4(Default::default());
    // let seed = 6667; // very random, very secure seed
    // let stack = &*singleton!(Stack::new(
    //     wifi_interface,
    //     config,
    //     singleton!(StackResources::<3>::new()),
    //     seed
    // ));

    // // let uart = &mut *singleton!(uart);

    // // Start tasks
    // let executor = EXECUTOR.init(Executor::new());
    // executor.run(|spawner| {
    //     // spawner
    //     //     .spawn(connection(controller, display_events_tx))
    //     //     .ok();
    //     // spawner.spawn(net_task(&stack)).ok();
    //     spawner.spawn(scanner_task(uart)).ok();
    //     // spawner.spawn(display_task(display)).ok();
    //     // spawner.spawn(task(&stack)).ok();
    // })

    let signals = &*mk_static!(Signals, Signals::default());
    let app_io = AppIO { signals, display: display.into() };
    let app = &*mk_static!(App, App::new(app_io));

    spawner.spawn(worker_task(&app)).unwrap();
    spawner.spawn(animator_task(&app)).unwrap();
    app.run().await;
}


#[embassy_executor::task]
async fn worker_task(app: &'static App) -> ! {
    app.worker().await;
}

#[embassy_executor::task]
async fn animator_task(app: &'static App) -> ! {
    app.animator().await;
}


#[embassy_executor::task]
async fn connection(mut controller: WifiController<'static>) {
    println!("start connection task");
    println!("Device capabilities: {:?}", controller.capabilities());
    loop {
        match esp_wifi::wifi::wifi_state() {
            WifiState::StaConnected => {
                // wait until we're no longer connected
                controller.wait_for_event(WifiEvent::StaDisconnected).await;
                Timer::after(Duration::from_millis(5000)).await
            }
            _ => {}
        }
        if !matches!(controller.is_started(), Ok(true)) {
            let client_config = Configuration::Client(ClientConfiguration {
                ssid: SSID.try_into().unwrap(),
                password: PASSWORD.try_into().unwrap(),
                ..Default::default()
            });
            controller.set_configuration(&client_config).unwrap();
            println!("Starting wifi");
            controller.start_async().await.unwrap();
            println!("Wifi started!");
        }
        println!("About to connect...");

        match controller.connect_async().await {
            Ok(_) => println!("Wifi connected!"),
            Err(e) => {
                println!("Failed to connect to wifi: {e:?}");
                Timer::after(Duration::from_millis(5000)).await
            }
        }
    }
}

#[embassy_executor::task]
async fn net_task(mut runner: Runner<'static, WifiDevice<'static>>) {
    runner.run().await
}

// type MySpi = SPIInterface<
//     Spi<'static, SPI2, FullDuplexMode>,
//     GpioPin<Output<PushPull>, 27>,
//     GpioPin<Output<PushPull>, 12>,
// >;
// type MyDisplay = mipidsi::Display<MySpi, ST7789, GpioPin<Output<PushPull>, 14>>;

// type MyUart = Uart<'static, UART2>;

// #[embassy_executor::task]
// async fn display_task(mut display: graphics::Display<MyDisplay>) {
//     loop {
//         display.update().unwrap();
//         Timer::after(Duration::from_millis(20)).await
//     }
// }

// mod sm {
//     use core::{marker::PhantomData, ops::Range};

//     use esp_println::println;

//     use crate::MyUart;

//     const HEADER_SIZE: usize = 3;
//     const DATA_SIZE: usize = 2953;
//     const MAX_SIZE: usize = HEADER_SIZE + DATA_SIZE;

//     pub trait Sized {
//         const OFFSET: usize;
//         const SIZE: usize;
//         fn max_range() -> Range<usize> { Self::OFFSET..(Self::OFFSET + Self::SIZE) }

//         fn length(&self) -> usize { Self::SIZE }
//         fn range(&self) -> Range<usize> { Self::OFFSET..(Self::OFFSET + self.length()) }
//     }
//     pub trait Checkable: Sized {
//         fn check(&self) -> bool {
//             true
//         }
//     }

//     pub trait Uarty<const D: usize>: Sized + Checkable {
//         async fn read(&mut self) -> Result<(usize, &[u8; D]), ()>;
//     }

//     pub struct Header;
//     impl Header { const CHAR: u8 = 0x03; }
//     pub struct Data(usize);

//     // Generic
//     pub struct StateMachine<S, const D: usize> {
//         uart: MyUart,
//         buffer: [u8; MAX_SIZE],
//         state: S,
//         _d: PhantomData<[(); D]>
//     }
//     impl<S, const D: usize> StateMachine<S, D> where StateMachine<S, D>: Sized {
//         fn reset(mut self) -> StateMachine<Header, HEADER_SIZE> {
//             self.uart.set_rx_fifo_full_threshold(HEADER_SIZE as u16).unwrap();
//             StateMachine { state: Header {}, _d: PhantomData, ..self }
//         }
//     }
//     impl<S, const D: usize> Uarty<D> for StateMachine<S, D> where StateMachine<S, D>: Sized + Checkable {
//         async fn read(&mut self) -> Result<(usize, &[u8; D]), ()> {
//             let mut range = self.range();
//             let mut total_size = 0;
//             while total_size < range.len() {
//                 match embedded_io_async::Read::read(&mut self.uart, &mut self.buffer[range.clone()]).await {
//                     Ok(size) => {
//                         range.start += size;
//                         total_size += size;
//                     }
//                     Err(esp32_hal::uart::Error::ReadBufferFull) => {
//                         total_size = self.length();
//                     }
//                     _ => {
//                         println!("Read failed");
//                         return Err(());
//                     }

//                 }
//             }
//             println!("max read {}", self.length());
//             println!("total_size read {total_size}");
//             Ok((total_size, self.buffer[Self::max_range()].try_into().unwrap()))
//         }
//     }

//     // Sync state
//     impl Sized for StateMachine<Header, HEADER_SIZE> {
//         const OFFSET: usize = 0;
//         const SIZE: usize = HEADER_SIZE;
//     }
//     impl StateMachine<Header, HEADER_SIZE> {
//         fn new(mut uart: MyUart) -> StateMachine<Header, HEADER_SIZE> {
//             uart.set_rx_fifo_full_threshold(Self::SIZE as u16).unwrap();
//             StateMachine { uart, buffer: [0u8; MAX_SIZE], state: Header {}, _d: PhantomData }
//         }
//     }
//     impl Checkable for StateMachine<Header, HEADER_SIZE> {
//         fn check(&self) -> bool {
//             self.buffer[Self::OFFSET] == Header::CHAR
//         }
//     }
//     impl StateMachine<Header, HEADER_SIZE> {
//         fn next(mut self, length: usize) -> StateMachine<Data, DATA_SIZE> {
//             self.uart.set_rx_fifo_full_threshold(length as u16).unwrap();
//             StateMachine { state: Data(length), _d: PhantomData, ..self }
//         }
//     }

//     // Data state
//     impl Sized for StateMachine<Data, DATA_SIZE> {
//         const OFFSET: usize = HEADER_SIZE;
//         const SIZE: usize = DATA_SIZE;
//         fn length(&self) -> usize { self.state.0 }
//     }
//     impl Checkable for StateMachine<Data,DATA_SIZE> {}
//     impl StateMachine<Data, DATA_SIZE> {
//         fn next(mut self) -> StateMachine<Header, HEADER_SIZE> {
//             self.uart.set_rx_fifo_full_threshold(HEADER_SIZE as u16).unwrap();
//             StateMachine { state: Header {}, _d: PhantomData, ..self }
//         }
//     }

//     pub(crate) enum StateMachineWrapper {
//         Sync(StateMachine<Header, HEADER_SIZE>),
//         Data(StateMachine<Data, DATA_SIZE>),
//     }
//     impl StateMachineWrapper {
//         pub fn new(uart: MyUart) -> Self {
//             StateMachineWrapper::Sync(StateMachine::new(uart))
//         }

//         // pub async fn read<const D: usize>(&mut self) -> Result<&[u8], ()>{
//         //     match self {
//         //         StateMachineWrapper::Sync(inner) => inner.read().await,
//         //         StateMachineWrapper::Length(inner) => inner.read().await,
//         //         StateMachineWrapper::Data(inner) => inner.read().await,
//         //     }
//         // }

//         pub async fn next(self) -> Self {
//             match self {
//                 StateMachineWrapper::Sync(mut inner) => {
//                     match inner.read().await {
//                         Ok((_, view)) => {
//                             if view[0] != Header::CHAR {
//                                 StateMachineWrapper::Sync(inner)
//                             } else {
//                                 let size = ((view[1] as usize)<< 8) + ((view[2] as usize));
//                                 StateMachineWrapper::Data(inner.next(size))
//                             }
//                         },
//                         Err(_) => StateMachineWrapper::Sync(inner),
//                     }
//                 },
//                 StateMachineWrapper::Data(mut inner) => {
//                     let expected_size = inner.state.0;
//                     match inner.read().await {
//                         Ok((size, view)) => {
//                             if size != expected_size {
//                                 println!("Wrong size");
//                                 return StateMachineWrapper::Sync(inner.reset());
//                             }
//                             let s = core::str::from_utf8(&view[..size]).unwrap();
//                             println!("{s}");
//                             StateMachineWrapper::Sync(inner.next())
//                         },
//                         Err(_) => StateMachineWrapper::Sync(inner.reset()),
//                     }
//                 },
//             }
//         }
//     }
// }

// #[embassy_executor::task]
// async fn scanner_task(mut uart: Uart<'static, UART2>) {
//     println!("scanner_task started");

//     const HEADER_SIZE: usize = 3;
//     const DATA_SIZE: usize = 2953;
//     const MAX_SIZE: usize = HEADER_SIZE + DATA_SIZE;

//     let mut buf = [0u8; MAX_SIZE as usize];
//     let mut offset = 0;
//     'outer: loop {
//         offset = 0;
//         // uart.set_rx_fifo_full_threshold(HEADER_SIZE as u16).unwrap();
//         loop {
//             match embedded_io_async::Read::read(&mut uart, &mut buf[offset..(offset+HEADER_SIZE)]).await {
//                 Err(hal::uart::Error::ReadBufferFull) => {
//                     offset += HEADER_SIZE;
//                     break;
//                 }
//                 Ok(size) => {
//                     if offset + size > HEADER_SIZE {
//                         println!("garbage");
//                         continue 'outer;
//                     }
//                     offset += size;
//                     println!("step 1: offset {offset}");
//                 }
//                 _ => {
//                     println!("Unexpected error");
//                     continue 'outer
//                 },
//             }
//         }

//         if buf[0] != 0x03 {
//             println!("Invalid sync char");
//             continue 'outer;
//         }

//         let payload_length = ((buf[1] as u16) << 8) + buf[2] as u16;
//         let mut bytes_left = payload_length;

//         loop {
//             let bytes_to_read = min(min(0x7F, payload_length), bytes_left);
//             println!("step 2: bytes_to_read {bytes_to_read}");
//             // uart.set_rx_fifo_full_threshold(bytes_to_read).unwrap();
//             match embedded_io_async::Read::read(&mut uart, &mut buf[offset..(offset + bytes_to_read as usize)]).await {
//                 Err(hal::uart::Error::ReadBufferFull) => {
//                     println!("step 2: buffer full (read {bytes_to_read} bytes)");
//                     bytes_left -= bytes_to_read;
//                     offset += bytes_to_read as usize;
//                 }
//                 Ok(size) => {
//                     bytes_left -= size as u16;
//                     offset += size;
//                     println!("step 2: read {size} bytes, waiting for {bytes_left} more");
//                 }
//                 _ => {
//                     println!("Unexpected error");
//                     continue 'outer;
//                 },
//             }
//             println!("step 2: offset {offset}");

//             if bytes_left == 0 {
//                 break;
//             }
//         }

//         println!("{}", core::str::from_utf8(&buf[offset-payload_length as usize..offset]).unwrap());
//     }

//     // let mut sm = sm::StateMachineWrapper::new(uart);
//     // loop {
//     //     sm = sm.next().await;
//     // }

//     // let mut state_sync = StateMachineWrapper::Sync(StateMachine::new(uart));
//     // loop {
//     //     let state_length = 'inner: loop {
//     //         let state_data = match state_sync.read().await {
//     //             Ok(_view) => state_sync.next(),
//     //             Err(_) => break 'inner state_sync.reset(),
//     //         };
//     //         break 'inner state_data.reset()
//     //         // let mut state_data = match state_length.read().await {
//     //         //     Ok(_view) => {
//     //         //         let length: usize = ((_view[0] as usize) << 8) + _view[1] as usize;
//     //         //         state_length.next(length);
//     //         //     }
//     //         //     Err(_) => return,
//     //         // };
//     //     };
//     // }
// }

// pub type EventsSender = Sender<'static, CriticalSectionRawMutex, Events, 10>;
// #[embassy_executor::task]
// async fn connection(mut controller: EspWifiController<'static>, display_events_tx: EventsSender) {
//     loop {
//         match esp_wifi::wifi::get_wifi_state() {
//             WifiState::StaConnected => {
//                 // wait until we're no longer connected
//                 controller.wait_for_event(WifiEvent::StaDisconnected).await;
//                 display_events_tx.send(Events::WifiDisconnected).await;
//                 Timer::after(Duration::from_secs(5)).await
//             }
//             _ => {}
//         }
//         if !matches!(controller.is_started(), Ok(true)) {
//             let client_config = Configuration::Client(ClientConfiguration {
//                 ssid: SSID.into(),
//                 password: PASSWORD.into(),
//                 ..Default::default()
//             });
//             controller.set_configuration(&client_config).unwrap();
//             println!("Starting wifi");
//             controller.start().await.unwrap();
//             println!("Wifi started!");
//         }
//         println!("About to connect to `{SSID}`...");

//         match controller.connect().await {
//             Ok(_) => {
//                 println!("Wifi connected!");
//                 display_events_tx.send(Events::WifiConnected).await;
//             }
//             Err(e) => {
//                 println!("Failed to connect to wifi: {e:?}");
//                 Timer::after(Duration::from_millis(5000)).await
//             }
//         }
//     }
// }

// #[embassy_executor::task]
// async fn net_task(stack: &'static Stack<WifiDevice<'static>>) {
//     stack.run().await
// }

// #[embassy_executor::task]
// async fn task(stack: &'static Stack<WifiDevice<'static>>) {
//     let mut rx_buffer = [0; 4096];
//     let mut tx_buffer = [0; 4096];

//     loop {
//         loop {
//             if stack.is_link_up() {
//                 break;
//             }
//             Timer::after(Duration::from_millis(500)).await;
//         }

//         println!("Waiting to get IP address...");
//         loop {
//             if let Some(config) = stack.config_v4() {
//                 println!("Got IP: {}", config.address);
//                 break;
//             }
//             Timer::after(Duration::from_millis(500)).await;
//         }

//         let address = loop {
//             match stack
//                 .dns_query("brgs.io", embassy_net::dns::DnsQueryType::A)
//                 .await
//             {
//                 Ok(addresses) => {
//                     let address = addresses[0];
//                     println!("{address}");
//                     break address;
//                 }
//                 Err(error) => {
//                     println!("DNS query failed, error: {error:?}");
//                     println!("retrying in 5 seconds...");
//                     Timer::after(Duration::from_secs(5)).await;
//                 }
//             }
//         };

//         loop {
//             Timer::after(Duration::from_secs(1)).await;

//             let mut socket = TcpSocket::new(&stack, &mut rx_buffer, &mut tx_buffer);

//             socket.set_timeout(Some(embassy_time::Duration::from_secs(10)));

//             let remote_endpoint = (address, 80);
//             let r = socket.connect(remote_endpoint).await;
//             if let Err(e) = r {
//                 println!("connect error: {:?}", e);
//                 continue;
//             }
//             let mut buf = [0; 1024];
//             loop {
//                 let r = socket
//                     .write_all(b"GET / HTTP/1.0\r\nHost: brgs.io\r\n\r\n")
//                     .await;
//                 if let Err(e) = r {
//                     println!("write error: {:?}", e);
//                     break;
//                 }
//                 let n = match socket.read(&mut buf).await {
//                     Ok(0) => {
//                         println!("read EOF");
//                         break;
//                     }
//                     Ok(n) => n,
//                     Err(e) => {
//                         println!("read error: {:?}", e);
//                         break;
//                     }
//                 };
//                 println!("{}", core::str::from_utf8(&buf[..n]).unwrap());
//             }
//             Timer::after(Duration::from_secs(3)).await;
//         }
//     }
// }

// // ! embassy serial
// // !
// // ! This is an example of running the embassy executor and asynchronously
// // ! writing to and reading from uart

// #![no_std]
// #![no_main]
// #![feature(type_alias_impl_trait)]

// use embassy_time::{with_timeout, Duration};
// use esp32_hal::{
//     clock::ClockControl,
//     embassy::{self, executor::Executor},
//     interrupt,
//     peripherals::{Interrupt, Peripherals, UART2},
//     prelude::*,
//     timer::TimerGroup,
//     Uart, uart::config::AtCmdConfig,
// };
// use esp_backtrace as _;
// use esp_println::{println, print};
// use static_cell::make_static;
// use heapless::Vec;
// use core::fmt::Write;

// // rx_fifo_full_threshold
// const READ_BUF_SIZE: usize = 64;
// // EOT (CTRL-D)
// const AT_CMD: u8 = 0x04;

// #[embassy_executor::task]
// async fn run(mut uart: Uart<'static, UART2>) {
//     // max message size to receive
//     // leave some extra space for AT-CMD characters
//     const MAX_BUFFER_SIZE: usize = 10 * READ_BUF_SIZE + 16;
//     // timeout read
//     const READ_TIMEOUT: Duration = Duration::from_secs(10);

//     let mut rbuf: Vec<u8, MAX_BUFFER_SIZE> = Vec::new();
//     let mut wbuf: Vec<u8, MAX_BUFFER_SIZE> = Vec::new();
//     loop {
//         if rbuf.is_empty() {
//             embedded_io_async::Write::write(
//                 &mut uart,
//                 b"Hello async serial. Enter something ended with EOT (CTRL-D).\r\n",
//             )
//             .await
//             .unwrap();
//         } else {
//             wbuf.clear();
//             write!(&mut wbuf, "\r\n-- received {} bytes --\r\n", rbuf.len()).unwrap();
//             embedded_io_async::Write::write(&mut uart, wbuf.as_slice())
//                 .await
//                 .unwrap();
//             embedded_io_async::Write::write(&mut uart, rbuf.as_slice())
//                 .await
//                 .unwrap();
//             embedded_io_async::Write::write(&mut uart, b"\r\n")
//                 .await
//                 .unwrap();
//         }
//         embedded_io_async::Write::flush(&mut uart).await.unwrap();

//         // set rbuf full capacity
//         rbuf.resize_default(rbuf.capacity()).ok();
//         let mut offset = 0;
//         loop {
//             match with_timeout(
//                 READ_TIMEOUT,
//                 embedded_io_async::Read::read(&mut uart, &mut rbuf[offset..]),
//             )
//             .await
//             {
//                 Ok(r) => {
//                     if let Ok(len) = r {
//                         offset += len;
//                         if offset == 0 {
//                             rbuf.truncate(0);
//                             break;
//                         }
//                         // if set_at_cmd is used than stop reading
//                         if len < READ_BUF_SIZE {
//                             rbuf.truncate(offset);
//                             break;
//                         }
//                     } else {
//                         // buffer is full or rx fifo overflow
//                         break;
//                     }
//                 }
//                 Err(_) => {
//                     // Timeout
//                     rbuf.truncate(offset);
//                     break;
//                 }
//             }
//         }
//     }
// }

// #[entry]
// fn main() -> ! {
//     esp_println::println!("Init!");
//     let peripherals = Peripherals::take();
//     let mut system = peripherals.DPORT.split();
//     let clocks = ClockControl::boot_defaults(system.clock_control).freeze();

//     let timer_group0 = TimerGroup::new(
//         peripherals.TIMG0,
//         &clocks,
//         &mut system.peripheral_clock_control,
//     );
//     embassy::init(&clocks, timer_group0.timer0);

//     // let io = esp32_hal::IO::new(peripherals.GPIO, peripherals.IO_MUX);
//     // let uart_config = esp32_hal::uart::config::Config::default().baudrate(9600);
//     // let uart_pins = esp32_hal::uart::TxRxPins::new_tx_rx(
//     //     io.pins.gpio17.into_push_pull_output(),
//     //     io.pins.gpio16.into_floating_input(),
//     // );
//     let uart2 = Uart::new(peripherals.UART2, &mut system.peripheral_clock_control);
//     // uart2.set_at_cmd(AtCmdConfig::new(None, None, None, AT_CMD, None));
//     // uart2
//     //     .set_rx_fifo_full_threshold(READ_BUF_SIZE as u16)
//     //     .unwrap();

//     interrupt::enable(Interrupt::UART2, interrupt::Priority::Priority1).unwrap();

//     let executor = make_static!(Executor::new());
//     executor.run(|spawner| {
//         spawner.spawn(run(uart2)).ok();
//     })
// }



/// Noop `OutputPin` implementation.
///
/// This is passed to `ExclusiveDevice`, because the CS pin is handle in
/// hardware.
struct NoCs;

impl embedded_hal::digital::OutputPin for NoCs {
    fn set_low(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }

    fn set_high(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }
}

impl embedded_hal::digital::ErrorType for NoCs {
    type Error = core::convert::Infallible;
}