# PTL Buddy

## Build

* Contained command

```
podman run --rm --interactive --tty --security-opt label=disable --userns keep-id --volume "$PWD":"$PWD" --workdir "$PWD" docker.io/espressif/idf-rust:esp32s3_latest sh -c '. ~/export-esp.sh && cargo install --path cross --root .'
```

* Invasive tooling

```
set -e
sudo dnf install --assumeyes rustup gcc perl
rustup init
. ~/.cargo/env
cargo install espup
espup install
. ~/export-esp.sh
cargo +esp install --path cross --root .
```

## Run simulator

```
cargo run
```
